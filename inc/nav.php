<div id="nav">
  <ul class="left">
    <li class="title"><a href="index.php">Typothèque libre</a></li>
    <li class="about"><a href="apropos.php">À propos</a></li>
    <li class="fontList">Fontes</li>
    <ul class="inside fontListInside">
      <?php
      foreach ($dirs as $dir){
        $txtFileInfos  = file_get_contents($path.'/'.$dir.'/infos.txt');
        if($txtFileInfos){
          $name     = extract_content($txtFileInfos, 'nom = ', ';');
        }
        ?>
        <li><a href="specimen.php?font=<?= $dir ?>"><?php if($txtFileInfos && $name) echo '<span>'.$name.'</span>' ?></a></li>';
      <?php } ?>
    </ul>
    <li class="fontList">Auteurs</li>
    <ul class="inside fontListInside">
    </ul>
    <li class="categories">Catégories</li>
    <ul class="inside categoriesInside">
      <li class="serif">Sérif<sup> 2</sup></li>
      <li class="sansSerif">Sans sérif<sup> 5</sup></li>
      <li class="monospace">Monospace<sup> 1</sup></li>
      <li class="script">Script<sup> 0</sup></li>
    </ul>
    <li class="tri">Trier</li>
    <ul class="inside insideTri">
      <li>a-z</li>
      <li>date</li>
      <li>chasse</li>
      <li>auteurs</li>
    </ul>
  </ul>
  <ul class="right">
    <li class="search"><input class="q" id="search_query" placeholder="Rechercher une fonte" type="text"></li>
    <li class="color">a</li>
    <div class="inside">
      <p>
        font color
      </p>
      <input class="colorpickerFont" data-wheelcolorpicker="" data-wcp-layout="block" type="text" data-wcp-snap="true">
      <p>
        background color
      </p>
      <input class="colorpickerBg" data-wheelcolorpicker="" data-wcp-layout="block" type="text" data-wcp-snap="true">	<span class="changeCol">ok</span> <span class="reset">reset</span>
    </div>
    <li class="fontSize"><input class="rangeValue" min="20" max="500" step="1" value="100" type="range"></li>
  </ul>
</div>
