<!DOCTYPE html>

<html>
  <head>
    <title>typothèque</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css" />
    <link rel="stylesheet" type="text/css" href="style/style.css" />
    <!-- <link rel="stylesheet/less" type="text/css" href="style/style.less" /> -->
    <link type="text/css" rel="stylesheet" href="js/lib/jquery-wheelcolorpicker/wheelcolorpicker.css" />
    <script type="text/javascript" src="js/lib/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/lib/opentype.min.js"></script>
    <script type="text/javascript" src="js/lib/jquery-wheelcolorpicker/jquery.wheelcolorpicker.js"></script>
  </head>
  <body>

<?php if($debug == true) ini_set('display_errors', 'On'); ?>
