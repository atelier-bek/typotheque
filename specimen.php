<?php

  $font     = $_GET['font'];

  $fontDir  = $font;

  include('inc/variables.php');
  include('inc/functions.php');
  include('inc/head.php');
  include('inc/nav.php');
  include('php/specNav.php');

  $files = array_diff(scandir($path.'/'.$fontDir), array('.', '..'));

  $fileNames  = array();
  $fileExts   = array();

  $fileInfos  = file_get_contents($path.'/'.$fontDir.'/infos.txt');

  if($fileInfos){

    // extraire les données du fichier texte (voir functions.php)
    $styles   = explode( ', ', extract_content($fileInfos, 'styles = ', ';'));
    $name     = extract_content($fileInfos, 'nom = ', ';');
    $cat      = extract_content($fileInfos, 'catégories = ', ';');
    $author   = extract_content($fileInfos, 'auteurs = ', ';');
    $link     = extract_content($fileInfos, 'lien = ', ';');
    $version  = extract_content($fileInfos, 'version = ', ';');
    $licence  = extract_content($fileInfos, 'licence = ', ';');
  }


  foreach($files as $file){

    $fileInfos  = pathinfo($file);
    $fontExt    = Array('ttf', 'otf');

    if( in_array($fileInfos[extension], $fontExt)){
      $fileName = $fileInfos[filename].'.'.$fileInfos[extension];
      $fileNames[] = $fileName;
    }
  }

  $nbFontes = count($fileNames);

?>
<div id="fontInfos" class="<?= $font ?>">

    <h1><?php if($fileInfos && $name) echo '<span>'.$name.'</span>' ?></h1>

    <h2>
      <?php
        echo $nbFontes;
        if($nbFontes == 1){
          echo ' style';
        } else {
          echo ' styles';
        }
      ?>:
      </h2>
    <ul class="<?php foreach($fileNames as $fileName) echo $fileName.' '; ?>"><?php foreach ($styles as $style) echo '<li>'.$style.'</li>' ?></ul>

    <h2> Catégorie(s):</h2>
    <ul class="cat"><li><?php if($fileInfos && $cat){ echo $cat; } else { echo '?'; } ?></li></ul>

    <h2> Auteur(s):</h2>
    <ul class="auteurs"><?php if($fileInfos && $author) echo '<li>'.$author.'</li>' ?></ul>

    <h2>Version:</h2>
    <ul class="version"><?php if($fileInfos && $version) echo '<li>'.$version.'</li>' ?></ul>

    <h2>Licence:</h2>
    <ul class="licence"><?php if($fileInfos && $licence) echo '<li>'.$licence.'</li>' ?></ul>

    <!-- <h2>Dernière modification:</h2>
    <ul class="modif"> </ul> -->

    <h2>Langages disponibles:</h2>
    <ul class="lang"> <li>?</li> </ul>

    <h2>Nombre de glyphes:</h2>
    <ul class="glyphes"> <li>?</li> </ul>

    <h2><a href="#">Télécharger</a></h2>
    <h2><a href="#">Imprimer un spécimen</a></h2>
  </div>

</div>

<div id="specimen">

    <?php
      foreach($fileNames as $fileName){

        $fontFamily = substr($fileName, 0, -4);
    ?>

      <style media="screen">
        @font-face {
          font-family: <?= $fontFamily ?>;
          src: url(<?php echo $path.'/'.$fontDir.'/'.$fileName; ?>);
        }
      </style>

      <div class="part waterfall" style="font-family: <?= $fontFamily ?>, substitut;">
        <h2><?= $fileName ?></h2>
        <?php for($i = 20; $i >= 1; $i--){ ?>
        <p class="sample <?php echo $fileName ?>"><input style=" font-size: <?= $i*8 ?>px;" type="text" value="Grimpez quand ce whisky flatte vos bijoux."></p>
        <p class="size">
          <?= $i*8 ?>px
        </p>
        <?php } ?>
      </div>

      <div class="part layout" style="font-family: <?= $fontFamily ?>, substitut;">
        <?php include("php/layout.php") ?>
      </div>

      <div class="part text" style="font-family: <?= $fontFamily ?>, substitut;">
        <h2><?= $fileName ?></h2>
        <?php for($i = 10; $i >= 1; $i--){ ?>
          <?php include("php/texte.php") ?>
          <p class="size">
            <?= $i*6 ?>px
          </p>
        <?php } ?>
      </div>

      <div class="part az" style="font-family: <?= $fontFamily ?>, substitut;">
        <?php include("php/az.php") ?>
      </div>

      <div class="part approche" style="font-family: <?= $fontFamily ?>, substitut;">
        <?php include("php/approche.php") ?>
      </div>

      <div class="part charMap" style="font-family: <?= $fontFamily ?>, substitut;">
        <h2><?= $fileName ?></h2>
        <p>
          <canvas id="canvas" width="800" height="800"></canvas>
        </p>
      </div>

      <div class="part vector" style="font-family: <?= $fontFamily ?>, substitut;">
        <h2><?= $fileName ?></h2>
        <p>
          soon
        </p>
      </div>

      <div class="part comp" style="font-family: <?= $fontFamily ?>, substitut;">
        <h2><?= $fileName ?></h2>
        <p>
          soon
        </p>
      </div>

    <?php } ?>




</div>

<?php include('inc/foot.php'); ?>
